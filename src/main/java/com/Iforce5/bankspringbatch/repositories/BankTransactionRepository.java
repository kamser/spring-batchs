package com.Iforce5.bankspringbatch.repositories;


import org.springframework.data.jpa.repository.JpaRepository;

import com.Iforce5.bankspringbatch.models.entities.BankTransaction;

public interface BankTransactionRepository  extends JpaRepository<BankTransaction, Long>{

 	
}
