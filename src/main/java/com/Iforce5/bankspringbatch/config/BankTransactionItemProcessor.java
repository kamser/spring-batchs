package com.Iforce5.bankspringbatch.config;

import java.text.SimpleDateFormat;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.Iforce5.bankspringbatch.models.entities.BankTransaction;

import net.bytebuddy.asm.Advice.Return;

@Component
public class BankTransactionItemProcessor implements ItemProcessor<BankTransaction,BankTransaction> {

	private SimpleDateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy-HH:mm");

	@Override
	public BankTransaction process(BankTransaction bankTransaction) throws Exception {
		
		bankTransaction.setTransactionDate(dateFormat.parse(bankTransaction.getStrTransaction()));
		
		return bankTransaction;
	}
	
	

	
	
}
